#pragma region includes
#include<iostream>
#include<queue>
#include<vector>
#include <chrono>

#include<unistd.h>
#pragma endregion

#pragma region namespaces
using namespace std;
using namespace std::chrono;
#pragma endregion

#pragma region MACROS
#define MEQ_INIT_SIZE 20
#define MEQ_SIM_COUNT 100 
#define MIN_RAND 1
#define MAX_RAND 10
#define RANDOM(l_lim,u_lim) rand()%u_lim +l_lim // generate random numbers between 1 and 10
#pragma endregion

#pragma region Event class and helper classes

class Event{
    public:
        int id;
        static int count;
        milliseconds timestamp;
        static milliseconds init_time;
    public:
        Event(){
            id = ++count;
            timestamp = duration_cast< milliseconds >(
                            system_clock::now().time_since_epoch()
                            ) - init_time;
        }
        
        virtual void execute() = 0;
};

int Event::count = 0;

milliseconds Event::init_time =  duration_cast< milliseconds >(
            system_clock::now().time_since_epoch()
            );


class IntEvent: public Event{
    public:
        int val;
    public:
        IntEvent(){
            val = RANDOM(MIN_RAND,MAX_RAND);
        }

        IntEvent(milliseconds& t, int v){
            srand(v);
            val = RANDOM(MIN_RAND,MAX_RAND);
            timestamp = t + milliseconds(val);
        }

        virtual void execute() {
            cout << "(" << timestamp.count() << " , " << val << ")" << endl;
        }

};



// Comparator class for Event objects to sort according to time
class EventComparator 
{ 
    public: 
        int operator() (const Event& e1, const Event& e2) 
        { 
            return e1.timestamp.count() > e2.timestamp.count(); 
        } 
};
#pragma endregion

#pragma region Helper functions
// Generate a set of events and add them in the queue
void generateEvents(priority_queue<IntEvent, vector<IntEvent>, EventComparator> &queue,int count){
    for(int i = 0; i< count;i++){
        IntEvent e = IntEvent();
        e.timestamp =  e.timestamp ;
        queue.push(e);
        usleep(2 * 1000);
    }
}

// Simulate the queue for set number of times and a add random event after processing each event
void simulateEvents(priority_queue<IntEvent, vector<IntEvent>, EventComparator> &queue,int count){
    for( int i=0;i<count;i++){
        IntEvent e = queue.top();
        e.execute();
        queue.pop();

        int v = rand()%10 + 1;
        queue.push(IntEvent(e.timestamp,e.val));
    }
}
#pragma endregion

#pragma region main function
int main(){
    // Set the seed to generate random numbers based on time
    srand(time(0));

    // IntEvent e;
    // e.execute();
    // e = IntEvent();
    // e.execute();



    // Generate a min heap for storing the events in a sorted queue based on time
    priority_queue<IntEvent, vector<IntEvent>, EventComparator> MEQ;

    // IntEvent e;
    // e.execute();
    // MEQ.push(e);
    // e = IntEvent();
    // e.execute();
    // MEQ.push(e);

    generateEvents(MEQ,MEQ_INIT_SIZE);

    simulateEvents(MEQ,MEQ_SIM_COUNT);

    return 0;
}
#pragma endregion