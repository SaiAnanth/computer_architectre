#ifndef __SIM4_H__
#define __SIM4_H__

#include "../cpu/cache.hh"
#include "../cpu/cpu.hh"

class simulation4 {
   public:
    System* s;
    Cpu* cpu0;
    Cpu* cpu1;
    RAM* Inst1Mem;
    RAM* Inst2Mem;
    RAM* Dataram;
    simulation4(/* args */);
    ~simulation4();
    void validate(bool);
    void run();
};

simulation4::simulation4(/* args */) {}

simulation4::~simulation4() {}

void simulation4::run() {
    uint32_t cpu0_inst[] = {
        0XFF010113, 0X00112623, 0X00812423, 0X01010413, 0X00000513, 0XFEA42A23,
        0XFEA42823, 0X0200006F, 0XFF042503, 0X0FF00593, 0X08A5C063, 0X0300006F,
        0X00000537, 0X40050513, 0XFF042583, 0X00259593, 0X00B50533, 0X00052007,
        0X00001537, 0X80050513, 0X00B50533, 0X00052087, 0X00100053, 0X00001537,
        0XC0050513, 0X00B50533, 0X00052027, 0X0700006F, 0XFF042503, 0X00150513,
        0XFEA42823, 0X0200006F, 0XFF442503, 0X00812403, 0X00C12083, 0X01010113,
        0X00008067};

    uint32_t cpu1_inst[] = {
        0XFF010113, 0X00112623, 0X00812423, 0X01010413, 0X00000513, 0XFEA42A23,
        0XFEA42823, 0X1200006F, 0XFF042503, 0X0FF00593, 0X18A5C063, 0X1300006F,
        0X00000537, 0X40050513, 0XFF042583, 0X00259593, 0X00B50533, 0X00052007,
        0X00001537, 0X80050513, 0X00B50533, 0X00052087, 0X00100053, 0X00001537,
        0X00050513, 0X00B50533, 0X00052027, 0X1700006F, 0XFF042503, 0X00150513,
        0XFEA42823, 0X1200006F, 0XFF442503, 0X00812403, 0X00C12083, 0X01010113,
        0X00008067};

    s = new System();
    cpu0 = new Cpu(s, "CPU_0");
    cpu0->setParams(0X2FF, 0X00);
    cpu0->setInstCache(256, 64, Associativity::DIRECT);
    cpu0->setDataCache(512, 64, Associativity::FULLY);
    // cpu0->init();

    cpu1 = new Cpu(s, "CPU_1");
    cpu1->setParams(0X3FF, 0X100);
    cpu1->setInstCache(256, 64, Associativity::DIRECT);
    cpu1->setDataCache(512, 64, Associativity::FULLY);
    // cpu1->init();

    RAM* Inst1ram = new RAM(s, AddrRange(0, 0XFF), 100);
    RAM* Inst2ram = new RAM(s, AddrRange(0X100, 0X1FF), 100);
    Dataram = new RAM(s, AddrRange(0X200, 0X1400), 100);

    Membus* bus = new Membus(s, 4);

    Inst1ram->setInstructions(cpu0_inst, 0X00, 37);
    Inst2ram->setInstructions(cpu1_inst, 0X100, 37);
    Dataram->setArrays(0X400, 256);
    Dataram->setArrays(0X800, 256);

    cpu0->getInstCachePort()->bind(bus->getUnboundCPUSidePort());
    cpu0->getDataCachePort()->bind(bus->getUnboundCPUSidePort());

    cpu1->getInstCachePort()->bind(bus->getUnboundCPUSidePort());
    cpu1->getDataCachePort()->bind(bus->getUnboundCPUSidePort());

    bus->getUnboundMemSidePort()->bind(Inst1ram->port);
    bus->getUnboundMemSidePort()->bind(Inst2ram->port);
    bus->getUnboundMemSidePort()->bind(Dataram->port);

    s->runSimulation(-1);
}

void simulation4::validate(bool with_comp = false) {
    if (with_comp) {
        float array_a[256], array_b[256], array_c[256];
        for (int i = 0; i < 256; i++) {
            array_a[i] = i;
            array_b[i] = i;
        }

        for (int i = 0; i < 256; i++) {
            array_c[i] = array_a[i] + array_b[i];
        }

        printf("\nverify the outputs of array_c with simulated values\n");
        printf("\ni     cal      CPU0      CPU1\n");
        for (int i = 0; i < 256; i++) {
            printf("%d %f %f %f\n", i, array_c[i],
                   Dataram->getFloat(0XC00 + (i * 4)),
                   Dataram->getFloat(0X1000 + (i * 4)));
        }
    }

    printf(
        "\nSimulation :For cache size of 1.5KiB we have allocated 256 Bytes "
        "\nfor Instructions and 512 Bytes for data caches with block size of "
        "\n64 Bytes. For Instruction cache we have taken direct Associative "
        "\nmapping and for Data Cache we have taken Fully Associative mapping. "
        "\nData Access time for memory is 100 ticks and cache lookup time is "
        "\nbased on the Associativity.\n");

    float cpu0cycles = cpu0->cycles;
    float cpu1cycles = cpu1->cycles;
    int32_t inst0cnt = cpu0->getInstructionCount();
    int32_t inst1cnt = cpu1->getInstructionCount();
    printf("\nCPI of CPU0 is %f\n", cpu0cycles / inst0cnt);
    printf("\nCPI of CPU1 is %f\n", cpu1cycles / inst1cnt);
    int32_t insts = inst0cnt + inst1cnt;
    printf("\nCPI of the entire system is %f\n",
           (float)(s->currTick() / 10) / insts);
}

#endif