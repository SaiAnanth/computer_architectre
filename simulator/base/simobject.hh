#ifndef __SIMOBJECT_H__
#define __SIMOBJECT_H__

#include "../system/system.hh"

class Simobject {
   protected:
    System *sys;
    Tick currTick() { return sys->currTick(); }
    void schedule(Event *e, Tick t) { sys->schedule(e, t); }
    void reschedule(Event *e, Tick t) { sys->reschedule(e, t); }

   public:
    Simobject(System *s) : sys(s) {}
    virtual void init() = 0;
    virtual void process() = 0;
};
#endif  // __SIMOBJECT_H__