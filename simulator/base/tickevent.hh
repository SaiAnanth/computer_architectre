#ifndef __TICKEVENT_H__
#define __TICKEVENT_H__

#include "../base/event.hh"
#include "../base/simobject.hh"

class TickEvent : public Event {
   private:
    Simobject *simobj;

   public:
    TickEvent(Simobject *obj) : simobj(obj) {}
    void process() { simobj->process(); }
    const char *description() { return "Tick Event"; }
};
#endif