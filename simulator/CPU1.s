main:               # Addr 0x100
	addi	sp, sp, -16					# 0XFF010113
	sw	ra, 12(sp)						# 0X00112623
	sw	s0, 8(sp)						# 0X00812423
	addi	s0, sp, 16					# 0X01010413
	mv	a0, zero						# 0X00000513
	sw	a0, -12(s0)						# 0XFEA42A23
	sw	a0, -16(s0)						# 0XFEA42823
	j	.LBB0_1		# jal PC, 0x120		# 0X1200006F
.LBB0_1:            # Addr 0x120
	lw	a0, -16(s0)						# 0XFF042503
	addi	a1, zero, 255				# 0X0FF00593
	blt	a1, a0, .LBB0_4					# 0X18A5C063
	j	.LBB0_2		# jal PC, 0x130		# 0X1300006F
.LBB0_2:            # Addr 0x130
	lui	a0, %hi(1024)					# 0X00000537
	addi	a0, a0, %lo(1024)			# 0X40050513
	lw	a1, -16(s0)						# 0XFF042583
	slli	a1, a1, 2					# 0X00259593
	add	a0, a0, a1						# 0X00B50533
	flw	ft0, 0(a0)						# 0X00052007
	lui	a0, %hi(2048)					# 0X00001537
	addi	a0, a0, %lo(2048)			# 0X80050513
	add	a0, a0, a1						# 0X00B50533
	flw	ft1, 0(a0)						# 0X00052087
	fsub.s	ft0, ft0, ft1				# 0X00100053
	lui	a0, %hi(4096)					# 0X00001537
	addi	a0, a0, %lo(4096)			# 0X00050513
	add	a0, a0, a1						# 0X00B50533
	fsw	ft0, 0(a0)						# 0X00052027
	j	.LBB0_3		# jal PC, 0x170		# 0X1700006F
.LBB0_3:            # Addr 0x170
	lw	a0, -16(s0)						# 0XFF042503
	addi	a0, a0, 1					# 0X00150513
	sw	a0, -16(s0)						# 0XFEA42823
	j	.LBB0_1 	# jal PC, 0x120		# 0X1200006F
.LBB0_4:		# Addr 0x180
	lw	a0, -12(s0)						# 0XFF442503
	lw	s0, 8(sp)						# 0X00812403
	lw	ra, 12(sp)						# 0X00C12083
	addi	sp, sp, 16					# 0X01010113
	ret									# 0X00008067