#ifndef __INSTCONST_H__
#define __INSTCONST_H__

#define BITMASK(m, n) ((uint32_t)-1 >> (32 - (n + 1))) & ((uint32_t)-1 << m)

#define GETBITS(x, m, n) (x & BITMASK(m, n)) >> m

#define SETBITS(x, y, m, n) \
    (x & ~(BITMASK(m, n))) | (y & BITMASK(0, n - m)) << m

#define SIGNEX(v, sb) ((v) | (((v) & (1 << (sb))) ? ~((1 << (sb)) - 1) : 0))
// RISCV RV32I Base Instruction Set

enum RV32ICode {
    LUI = 0b0110111,
    AUIPC = 0b0010111,
    ADDI = 0b0010011,
    SLTI = 0b0010011,
    SLTIU = 0b0010011,
    XORI = 0b0010011,
    ORI = 0b0010011,
    ANDI = 0b0010011,
    SLLI = 0b0010011,
    SRLI = 0b0010011,
    SRAI = 0b0010011,

    SW = 0b0100011,
    LW = 0b0000011,

    ADD = 0b0110011,
    JAL = 0b1101111,
    JALR = 0b1100111,

    BLT = 0b1100011,
};

enum RV32FCode {
    FLW = 0b0000111,
    FADDS = 0b1010011,
    FSW = 0b0100111,
};

#endif  // __INSTCONST_H__