#ifndef __RV32F_H__
#define __RV32F_H__

#include "../mem/packet.hh"
#include "itype.hh"
#include "registers.hh"
#include "rtype.hh"
#include "sbtype.hh"
#include "stype.hh"
#include "ujtype.hh"
#include "utype.hh"

class Flw : public Itype {
   public:
    bool read_write_mode = false;
    Flw(uint32_t* regs, float* fregs, uint32_t inst) : Itype(regs, fregs) {
        stall = 5;
        reg = inst;
    }
    virtual PipeLineStatus read_write() override {
        printf("requesting float data from %X\n", regs[rs1]);
        Packet* pkt = new Packet(false, regs[rs1] + imm, 4);
        dport->sendReq(pkt);
        return READWRITE;
    }

    virtual PipeLineStatus execute() override {
        executed = true;
        return COMPLETED;
    }

    virtual void store() override {
        fregs[rd] = fresult;
        printf("FLW loaded value %f in %d from address %X + %X \n", fresult, rd,
               regs[rs1], imm);
        stored = true;
    }
};

#pragma region Rtype

class Fadds : public Rtype {
   public:
    Fadds(uint32_t* regs, float* fregs, uint32_t inst) : Rtype(regs, fregs) {
        stall = 5;
        reg = inst;
    }

    virtual PipeLineStatus execute() override {
        printf("adding %f and %f from %d and %d\n", fregs[rs1], fregs[rs2], rs1,
               rs2);
        fresult = fregs[rs1] + fregs[rs2];
        executed = true;
        return STALLING;
    }

    virtual void store() override {
        printf("float saving in reg %f in %d\n", fresult, rd);
        fregs[rd] = fresult;
        stored = true;
    }
};

#pragma endregion Rtype

#pragma region Stype

class Fsw : public Stype {
   public:
    bool read_write_mode = false;
    Fsw(uint32_t* regs, float* fregs, uint32_t inst) : Stype(regs, fregs) {
        stall = 5;
        reg = inst;
    }
    virtual PipeLineStatus read_write() override {
        Packet* pkt = new Packet(true, regs[rs1] + imm, 4);
        *(float*)pkt->data = fregs[rs2];
        dport->sendReq(pkt);
        return READWRITE;
    }

    virtual PipeLineStatus execute() override {
        executed = true;
        printf(
            "execute instruction %X :  float mem write reg[%d]s value %f  in "
            "reg[%d]+%d "
            "address %X\n",
            reg, rs2, fregs[rs2], rs1, imm, regs[rs1] + imm);
        return COMPLETED;
    }

    virtual void store() override { stored = true; }
    void dispInst() override { printf("sw %d, %d(%d)\n", rs2, imm, rs1); }
};

#pragma endregion Stype

#endif  // __RV32F_H__