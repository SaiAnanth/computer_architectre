#ifndef __RV32I_H__
#define __RV32I_H__

#include "../mem/packet.hh"
#include "itype.hh"
#include "registers.hh"
#include "rtype.hh"
#include "sbtype.hh"
#include "stype.hh"
#include "ujtype.hh"
#include "utype.hh"

#pragma region Itype

class Addi : public Itype {
   public:
    Addi(uint32_t* regs, float* fregs, uint32_t inst) : Itype(regs, fregs) {
        stall = 0;
        reg = inst;
    }

    virtual PipeLineStatus execute() override {
        result = regs[rs1] + imm;
        executed = true;
        printf("execute instruction %X :  add reg[%d]+%d \n", reg, rs1, imm);
        return STALLING;
    }

    virtual void store() override {
        regs[rd] = result;
        stored = true;
        printf("Store instruction %X :  stored %X in reg[%d] \n", reg, result,
               rd);
    }
    void dispInst() override { printf("addi %d, %d, %d\n", rd, rs1, imm); }
};

class Andi : public Itype {
   public:
    Andi(uint32_t* regs, float* fregs, uint32_t inst) : Itype(regs, fregs) {
        stall = 0;
        reg = inst;
    }

    virtual PipeLineStatus execute() override {
        result = regs[rs1] & imm;
        executed = true;
        return STALLING;
    }

    virtual void store() override {
        regs[rd] = result;
        stored = true;
    }
};

class Lw : public Itype {
   public:
    bool read_write_mode = false;
    Lw(uint32_t* regs, float* fregs, uint32_t inst) : Itype(regs, fregs) {
        stall = 0;
        reg = inst;
    }
    virtual PipeLineStatus read_write() override {
        Packet* pkt = new Packet(false, regs[rs1] + imm, 4);
        dport->sendReq(pkt);
        return READWRITE;
    }

    virtual PipeLineStatus execute() override {
        executed = true;
        return COMPLETED;
    }

    virtual void store() override {
        regs[rd] = result;
        stored = true;
    }
};

class Slli : public Itype {
   public:
    Slli(uint32_t* regs, float* fregs, uint32_t inst) : Itype(regs, fregs) {
        stall = 0;
        reg = inst;
    }

    virtual PipeLineStatus execute() override {
        result = regs[rs1] << imm;
        executed = true;
        return STALLING;
    }

    virtual void store() override {
        regs[rd] = result;
        stored = true;
    }
};
class Jalr : public Itype {
   public:
    Jalr(uint32_t* regs, float* fregs, uint32_t inst) : Itype(regs, fregs) {
        stall = 0;
        reg = inst;
    }

    virtual PipeLineStatus execute() override {
        result = imm;
        if (rs1 == Register::ra) {
            printf("executing ret %d\n", rs1);
            ret = true;
        }
        executed = true;
        return STALLING;
    }

    virtual void store() override {
        regs[rd] = result;
        stored = true;
    }
};

#pragma endregion Itype

#pragma region Rtype

class Add : public Rtype {
   public:
    Add(uint32_t* regs, float* fregs, uint32_t inst) : Rtype(regs, fregs) {
        stall = 0;
        reg = inst;
    }

    virtual PipeLineStatus execute() override {
        result = regs[rs1] + regs[rs2];
        executed = true;
        return STALLING;
    }

    virtual void store() override {
        regs[rd] = result;
        stored = true;
    }
};

class And : public Rtype {
   public:
    And(uint32_t* regs, float* fregs, uint32_t inst) : Rtype(regs, fregs) {
        stall = 0;
        reg = inst;
    }

    virtual PipeLineStatus execute() override {
        result = regs[rs1] & regs[rs2];
        executed = true;
        return STALLING;
    }

    virtual void store() override {
        regs[rd] = result;
        stored = true;
    }
};

#pragma endregion Rtype

#pragma region Utype

class Lui : public Utype {
   public:
    Lui(uint32_t* regs, float* fregs, uint32_t inst) : Utype(regs, fregs) {
        stall = 0;
        reg = inst;
    }

    virtual PipeLineStatus execute() override {
        result = imm;
        SETBITS(result, 0, 0, 11);
        executed = true;
        return STALLING;
    }

    virtual void store() override {
        regs[rd] = result;
        stored = true;
    }
};

#pragma endregion Utype

#pragma region Stype

class Sw : public Stype {
   public:
    bool read_write_mode = false;
    Sw(uint32_t* regs, float* fregs, uint32_t inst) : Stype(regs, fregs) {
        stall = 0;
        reg = inst;
    }
    virtual PipeLineStatus read_write() override {
        Packet* pkt = new Packet(true, regs[rs1] + imm, 4);
        *(uint32_t*)pkt->data = regs[rs2];
        dport->sendReq(pkt);
        return READWRITE;
    }

    virtual PipeLineStatus execute() override {
        executed = true;
        printf(
            "execute instruction %X :  mem write reg[%d]s value %X  in "
            "reg[%d]+%d "
            "address %X\n",
            reg, rs2, regs[rs2], rs1, imm, regs[rs1] + imm);
        return COMPLETED;
    }

    virtual void store() override { stored = true; }
    void dispInst() override { printf("sw %d, %d(%d)\n", rs2, imm, rs1); }
};

#pragma endregionStype

#pragma region UJType

class Jal : public UJtype {
   public:
    Jal(uint32_t* regs, float* fregs, uint32_t inst) : UJtype(regs, fregs) {
        stall = 0;
        reg = inst;
    }

    virtual PipeLineStatus execute() override {
        result = imm;
        if (rd == 0) {
            regs[Register::PC] = result;
            isjump = true;
        }
        executed = true;
        return STALLING;
    }

    virtual void store() override {
        regs[rd] = result;
        stored = true;
    }
};

#pragma endregion UJType

#pragma region SBType
class Blt : public SBtype {
   public:
    Blt(uint32_t* regs, float* fregs, uint32_t inst) : SBtype(regs, fregs) {
        stall = 0;
        reg = inst;
    }

    virtual PipeLineStatus execute() override {
        result = 0;
        printf("cmp BLT\n");
        if (regs[rs1] < regs[rs2]) {
            isjump = true;
            regs[Register::PC] = imm;
        }
        executed = true;
        return STALLING;
    }

    virtual void store() override { stored = true; }
};

#pragma endregion SBType

#endif