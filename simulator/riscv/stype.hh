#ifndef __STYPE_H__
#define __STYPE_H__
#include "instruction.hh"

class Stype : public Instruction {
   public:
    Stype(uint32_t* regs, float* fregs) : Instruction(regs, fregs) {}
    virtual void decode() {
        code = GETBITS(reg, 0, 6);
        imm = SETBITS(imm, GETBITS(reg, 7, 11), 0, 4);
        func3 = GETBITS(reg, 12, 14);
        rs1 = GETBITS(reg, 15, 19);
        rs2 = GETBITS(reg, 20, 24);
        imm = SIGNEX(SETBITS(imm, GETBITS(reg, 25, 31), 5, 11), 11);
    }

    void dispInst() {
        printf("code: %X func3: %X rs1: %X rs2: %X imm: %d\n", code, func3, rs1,
               rs2, imm);
    }

    uint32_t static generate(uint8_t c, uint8_t f3, uint8_t rs1, uint8_t rs2,
                             int32_t i) {
        uint32_t r = 0;
        r = SETBITS(r, c, 0, 6);
        r = SETBITS(r, GETBITS(i, 0, 4), 7, 11);
        r = SETBITS(r, f3, 12, 14);
        r = SETBITS(r, rs1, 15, 19);
        r = SETBITS(r, rs2, 20, 24);
        r = SETBITS(r, GETBITS(i, 5, 11), 25, 31);
        return r;
    }
};

#endif  // __STYPE_H__