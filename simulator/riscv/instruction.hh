#ifndef __INSTRUCTION_H__
#define __INSTRUCTION_H__

#include <cstdint>
#include <string>

#include "../cpu/pipeline_status.hh"
#include "../mem/port.hh"
#include "inst_const.hh"

class Instruction {
   protected:
    uint8_t code;
    uint8_t rd;
    uint8_t rs1;
    uint8_t rs2;
    uint8_t func3;
    uint8_t func7;
    int32_t imm;

    bool fetched;
    bool decoded;
    bool executed;
    bool stored;

   public:
    bool isjump;
    bool ret;
    uint32_t reg;
    uint32_t* regs;
    float* fregs;
    MasterPort* dport;
    uint32_t result;
    float fresult;
    int stall;

    Instruction(uint32_t* r, float* fr)
        : regs(r), fregs(fr), reg(0), stall(0) {}

    void setReg(uint32_t r) { reg = r; }
    uint32_t getReg() { return reg; }

    virtual void fetch() { fetched = true; }
    virtual void decode() { decoded = true; }
    virtual PipeLineStatus read_write() { return READWRITECOMPLETE; }
    virtual PipeLineStatus execute() {
        executed = true;
        return COMPLETED;
    }
    virtual void store() { stored = true; }

    virtual void dispInst() {}

    bool isFetched() { return fetched; }
    bool isDecoded() { return decoded; }
    bool isExecuted() { return executed; }
    bool checkStall() { return stall <= 0; }
    void wait() {
        if (stall > 0) {
            stall--;
        };
    }

    bool isempty() { return reg == 0; }
};

#endif