#ifndef __ITYPE_H__
#define __ITYPE_H__
#include "instruction.hh"

class Itype : public Instruction {
   public:
    Itype(uint32_t* regs, float* fregs) : Instruction(regs, fregs) {}
    virtual void decode() override {
        code = GETBITS(reg, 0, 6);
        rd = GETBITS(reg, 7, 11);
        func3 = GETBITS(reg, 12, 14);
        rs1 = GETBITS(reg, 15, 19);
        imm = SIGNEX(GETBITS(reg, 20, 31), 11);
    }

    void dispInst() {
        printf("code: %X func3: %d rd: %d rs1: %d imm: %d\n", code, func3, rd,
               rs1, imm);
    }

    uint32_t static generate(uint8_t c, uint8_t f3, uint8_t rd, uint8_t rs1,
                             int32_t i) {
        uint32_t r = 0;
        r = SETBITS(r, c, 0, 6);
        r = SETBITS(r, rd, 7, 11);
        r = SETBITS(r, f3, 12, 14);
        r = SETBITS(r, rs1, 15, 19);
        r = SETBITS(r, GETBITS(i, 0, 11), 20, 31);
        return r;
    }
};

#endif  // __ITYPE_H__