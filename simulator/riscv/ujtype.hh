#ifndef __UJTYPE_H__
#define __UJTYPE_H__
#include "instruction.hh"

class UJtype : public Instruction {
   public:
    UJtype(uint32_t* regs, float* fregs) : Instruction(regs, fregs) {}
    virtual void decode() {
        code = GETBITS(reg, 0, 6);
        rd = GETBITS(reg, 7, 11);
        imm = SETBITS(imm, GETBITS(reg, 12, 19), 12, 19);
        imm = SETBITS(imm, GETBITS(reg, 20, 20), 11, 11);
        imm = SETBITS(imm, GETBITS(reg, 21, 30), 01, 10);
        imm = SETBITS(imm, GETBITS(reg, 31, 31), 20, 20);
    }
    void dispInst() { printf(" %X %X %X\n", imm, rd, code); }

    uint32_t static generate(uint8_t c, uint8_t rd, int32_t i) {
        uint32_t r = 0;
        r = SETBITS(r, c, 0, 6);
        r = SETBITS(r, rd, 7, 11);
        r = SETBITS(r, GETBITS(i, 12, 19), 12, 19);
        r = SETBITS(r, GETBITS(i, 11, 11), 20, 20);
        r = SETBITS(r, GETBITS(i, 1, 10), 21, 30);
        r = SETBITS(r, GETBITS(i, 20, 20), 31, 31);
        return r;
    }
};

#endif  // __UTYPE_H__