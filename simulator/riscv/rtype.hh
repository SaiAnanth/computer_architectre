#ifndef __RTYPE_H__
#define __RTYPE_H__
#include "instruction.hh"

class Rtype : public Instruction {
   public:
    Rtype(uint32_t* regs, float* fregs) : Instruction(regs, fregs) {}
    virtual void decode() {
        code = GETBITS(reg, 0, 6);
        rd = GETBITS(reg, 7, 11);
        func3 = GETBITS(reg, 12, 14);
        rs1 = GETBITS(reg, 15, 19);
        rs2 = GETBITS(reg, 20, 24);
        func7 = GETBITS(reg, 25, 31);
    }
    void dispInst() {
        printf("%X %X %X %X %X %X\n", func7, rs2, rs1, func3, rd, code);
    }

    uint32_t static generate(uint8_t c, uint8_t rd, uint8_t f3, uint8_t rs1,
                             uint8_t rs2, uint8_t f7) {
        uint32_t r = 0;
        r = SETBITS(r, c, 0, 6);
        r = SETBITS(r, rd, 7, 11);
        r = SETBITS(r, f3, 12, 14);
        r = SETBITS(r, rs1, 15, 19);
        r = SETBITS(r, rs2, 20, 24);
        r = SETBITS(r, f7, 25, 31);
        return r;
    }
};

#endif  // __RTYPE_H__