#ifndef __SBTYPE_H__
#define __SBTYPE_H__
#include "instruction.hh"

class SBtype : public Instruction {
   public:
    SBtype(uint32_t* regs, float* fregs) : Instruction(regs, fregs) {}
    virtual void decode() {
        code = GETBITS(reg, 0, 6);
        func3 = GETBITS(reg, 12, 14);
        rs1 = GETBITS(reg, 15, 19);
        rs2 = GETBITS(reg, 20, 24);

        imm = SETBITS(imm, GETBITS(reg, 7, 7), 11, 11);
        imm = SETBITS(imm, GETBITS(reg, 8, 11), 1, 4);
        imm = SETBITS(imm, GETBITS(reg, 25, 30), 5, 10);
        imm = SETBITS(imm, GETBITS(reg, 31, 31), 12, 12);
    }

    void dispInst() { printf("%X %X %X %X %X\n", imm, rs2, rs1, func3, code); }
    uint32_t static generate(uint8_t c, uint8_t f3, uint8_t rs1, uint8_t rs2,
                             int32_t i) {
        uint32_t r = 0;
        r = SETBITS(r, c, 0, 6);
        r = SETBITS(r, f3, 12, 14);
        r = SETBITS(r, rs1, 15, 19);
        r = SETBITS(r, rs2, 20, 24);
        r = SETBITS(r, GETBITS(i, 11, 11), 7, 7);
        r = SETBITS(r, GETBITS(i, 1, 4), 8, 11);
        r = SETBITS(r, GETBITS(i, 5, 10), 25, 30);
        r = SETBITS(r, GETBITS(i, 12, 12), 31, 31);
        return r;
    }
};

#endif  // __STYPE_H__