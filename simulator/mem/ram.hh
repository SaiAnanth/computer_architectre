#ifndef __RAM_H__
#define __RAM_H__

#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "../base/simobject.hh"
#include "port.hh"

class RAM : public Simobject {
   private:
    class RamDPort : public SlavePort {
       private:
        AddrRange range;
        RAM* ram;

       public:
        RamDPort(RAM* obj, AddrRange range)
            : SlavePort("RAM Data Port"), range(range), ram(obj) {}
        virtual void recvReq(Packet* pkt) override {
            assert(pkt->addr >= range.first &&
                   (pkt->addr + pkt->size) <= range.second);
            DPortEvent* e = new DPortEvent(ram, pkt);
            ram->schedule(e, ram->currTick() + ram->latency);
        }
        const AddrRange getAddrRange() { return range; }
    };

    class DPortEvent : public Event {
       public:
        RAM* ram;
        Packet* pkt;
        DPortEvent(RAM* r, Packet* p) : ram(r), pkt(p) {}
        virtual void process() {
            if (!pkt) {
                return;
            }
            if (pkt->isWrite()) {
                for (int i = 0; i < pkt->size; i += 4) {
                    memcpy((ram->mem + pkt->addr + i), (pkt->data + i), 4);
                }
                // memcpy((ram->mem + pkt->addr), pkt->data, pkt->size);
            } else {
                for (int i = 0; i < pkt->size; i += 4) {
                    memcpy((pkt->data + i), (ram->mem + pkt->addr + i), 4);
                }
                // memcpy(pkt->data, (ram->mem + pkt->addr), pkt->size);
            }
            ram->port->sendResp(pkt);
        }
        const char* description() override { return "Mem access event"; }
    };

    uint8_t* mem;
    AddrRange addrRange;
    uint32_t latency;

   public:
    RamDPort* port;
    RAM(System* s, AddrRange range, uint32_t latency = 20)
        : Simobject(s),
          addrRange(range),
          latency(latency),
          mem(new u_int8_t[range.second]),
          port(new RamDPort(this, range)) {}

    virtual void init() {}
    virtual void process() {}

    float getFloat(uint32_t addr) { return *(float*)(mem + addr); }

    void setArrays(uint32_t start_addr, uint32_t count) {
        for (int i = 0; i < count; i++) {
            *(float*)(mem + start_addr + (i * 4)) = i;
        }
    }

    void printArray(uint32_t start_addr, uint32_t count) {
        for (int i = 0; i < count; i++) {
            printf("%d %f \n", i, *(float*)(mem + start_addr + (i * 4)));
        }
    }

    void setInstructions(uint32_t* ptr, uint32_t start_addr, uint32_t count) {
        for (int i = 0; i < count; i++) {
            *(u_int32_t*)(mem + start_addr + (i * 4)) = ptr[i];
        }
    }
};
#endif  // __RAM_H__