#include "membus.hh"

#include <assert.h>

#include <iostream>

bool AddressInRange(uint32_t ad, AddrRange ar) {
    return (ad >= ar.first) && (ad <= ar.second);
}

Membus::Membus(System *sys, Tick forward_time = 0)
    : Simobject(sys),
      fwd_time(forward_time),
      fwd_evnt(new MembusForwardEvent(this)) {
    memSidePorts.push_back(new MemSidePort(this));
    cpuSidePorts.push_back(new CPUSidePort(this));
}

void Membus::tryToSend() {
    auto it = packetsWaitingForMemPorts.begin();
    while (it != packetsWaitingForMemPorts.end()) {
        Packet *pkt = *it;
        MemSidePort *mem_port = getRequestPort(pkt);

        if (!(mem_port->isBusy())) {
            printf("Membus sending packet on Tick: %ld \n", currTick());
            mem_port->sendReq(pkt);
            it = packetsWaitingForMemPorts.erase(it);
        } else {
            it++;
        }
    }
}
void Membus::forwardPackets() {
    Tick curr = currTick();
    while (!(packetsWaitingForForward.empty()) &&
           (packetsWaitingForForward.front().first == curr)) {
        fwdQType tmp = packetsWaitingForForward.front();
        packetsWaitingForForward.pop_front();
        packetsWaitingForMemPorts.push_back(tmp.second);
    }
    if (!(packetsWaitingForForward.empty())) {
        fwdQType tmp = packetsWaitingForForward.front();
        printf("forward event scheduled at %ld\n", tmp.first);
        schedule(fwd_evnt, tmp.first);
    }
    tryToSend();
}
// put the packet in the queue and schedule an event
void Membus::recvReq(Packet *pkt) {
    Tick forwardTick = currTick() + fwd_time;
    packetsWaitingForForward.push_back(fwdQType(forwardTick, pkt));

    if (!fwd_evnt->isScheduled()) {
        printf("forward event echeduled at %ld\n", forwardTick);
        schedule(fwd_evnt, forwardTick);
    }
    tryToSend();
}
// send the received response to respective port
void Membus::recvResp(Packet *pkt) {
    CPUSidePort *resp_port = getResponsePort(pkt);
    resp_port->sendResp(pkt);
    tryToSend();
}

Membus::MemSidePort *Membus::getRequestPort(Packet *pkt) {
    for (auto port : memSidePorts) {
        if (AddressInRange(pkt->addr, port->getAddrRange())) return port;
    }
    printf("Couldn't find the destination port for the packet for addr %X\n",
           pkt->addr);
    assert(0);
    return NULL;
}
Membus::CPUSidePort *Membus::getResponsePort(Packet *pkt) {
    for (auto port : cpuSidePorts) {
        if (port->getMaster() == pkt->getHeaderEnd()) return port;
    }
    std::cout << "Couldn't find the original port for the packet";
    assert(0);
    return NULL;
}
Membus::MemSidePort *Membus::getMemSidePort(size_t index) {
    if (index < memSidePorts.size()) return memSidePorts.at(index);
    std::cout << "Couldn't find valid port at index:" << index << std::endl;
    assert(0);
    return NULL;
}
Membus::CPUSidePort *Membus::getCPUSidePort(size_t index) {
    if (index < cpuSidePorts.size()) return cpuSidePorts.at(index);
    std::cout << "Couldn't find valid port at index:" << index << std::endl;
    assert(0);
    return NULL;
}
Membus::MemSidePort *Membus::getUnboundMemSidePort() {
    if (memSidePorts.back()->isBound())
        memSidePorts.push_back(new MemSidePort(this));
    return memSidePorts.back();
}
Membus::CPUSidePort *Membus::getUnboundCPUSidePort() {
    if (cpuSidePorts.back()->isBound())
        cpuSidePorts.push_back(new CPUSidePort(this));
    return cpuSidePorts.back();
}