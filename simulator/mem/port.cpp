#include "port.hh"

void SlavePort::bind(MasterPort* _mport) {
    mport = _mport;
    if (mport->getSlave() != this) mport->bind(this);
}
void SlavePort::unbind() { mport = NULL; }
void SlavePort::sendResp(Packet* pkt) {
    pkt->popHeader();
    activeRequest = NULL;
    mport->activeRequest = NULL;
    mport->recvResp(pkt);
}

void MasterPort::bind(SlavePort* _sport) {
    sport = _sport;
    if (sport->getMaster() != this) sport->bind(this);
}
void MasterPort::unbind() {
    sport->unbind();
    sport = NULL;
}
void MasterPort::sendReq(Packet* pkt) {
    pkt->appendHeader(this);
    activeRequest = pkt;
    sport->activeRequest = pkt;
    // printf("testing %s\n", sport->name.c_str());
    sport->recvReq(pkt);
}