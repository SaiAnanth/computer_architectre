#ifndef __PACKET_H__
#define __PACKET_H__

#include <bitset>
#include <vector>

typedef std::pair<uint32_t, uint32_t> AddrRange;

class Port;
class MasterPort;

class Packet {
   private:
    bool iswrite;

   public:
    std::vector<MasterPort*> path;
    int size;
    int addr;
    uint8_t* data;

   public:
    Packet(bool isWrite, int addr, int size)
        : iswrite(isWrite), addr(addr), size(size), data(new u_int8_t[size]) {}
    bool isWrite() { return iswrite; }

    void appendHeader(MasterPort* port) { path.push_back(port); }
    void popHeader() { path.pop_back(); }
    MasterPort* getHeaderEnd() { return path.back(); }
};

#endif  // __PACKET_H__