#ifndef __MEMBUS_HH__
#define __MEMBUS_HH__

#include <deque>
#include <vector>

#include "../base/simobject.hh"
#include "port.hh"
#include "ram.hh"

class Membus : public Simobject {
   private:
    /*************************************************************************
     * Packets take time to filter through the membus. This event manages this
     * process.
     *************************************************************************/
    class MembusForwardEvent : public Event {
       private:
        Membus *owner;

       public:
        MembusForwardEvent(Membus *_owner) : Event(), owner(_owner) {}
        void process() override { owner->forwardPackets(); }
        const char *description() override { return "Membus Forward Event"; }
    };

    /*************************************************************************
     * Port that points downward toward DRAM in the memory hierarchy
     *************************************************************************/
    class MemSidePort : public MasterPort {
       private:
        Membus *owner;

       public:
        Packet *activeRequest;
        MemSidePort(Membus *_owner)
            : MasterPort("MEMbus DRAM side port"), owner(_owner) {}
        bool isBusy() { return (activeRequest != NULL); }
        void recvResp(Packet *pkt) override {
            activeRequest = NULL;
            owner->recvResp(pkt);
        }
        void sendReq(Packet *pkt) override {
            activeRequest = pkt;
            MasterPort::sendReq(pkt);
        }
    };
    /*************************************************************************
     * Port that points upward toward a CPU in the memory hierarchy
     *************************************************************************/
    class CPUSidePort : public SlavePort {
       private:
        Membus *owner;

       public:
        CPUSidePort(Membus *_owner)
            : SlavePort("MEMBU CPU side port"), owner(_owner) {}
        virtual void recvReq(Packet *pkt) override {
            printf("membus cpu side request\n");
            owner->recvReq(pkt);
        }
        const AddrRange getAddrRange() override { return AddrRange(0, 0); }
    };

    typedef std::pair<Tick, Packet *> fwdQType;

    Tick fwd_time;
    MembusForwardEvent *fwd_evnt;
    std::vector<MemSidePort *> memSidePorts;
    std::vector<CPUSidePort *> cpuSidePorts;
    std::deque<Packet *> packetsWaitingForMemPorts;
    std::deque<fwdQType> packetsWaitingForForward;

   public:
    Membus(System *sys, Tick forward_time);

    void process() {}
    void init() {}

    void tryToSend();
    void forwardPackets();
    void recvReq(Packet *pkt);
    void recvResp(Packet *pkt);
    MemSidePort *getRequestPort(Packet *pkt);
    CPUSidePort *getResponsePort(Packet *pkt);

    MemSidePort *getMemSidePort(size_t index);
    CPUSidePort *getCPUSidePort(size_t index);
    MemSidePort *getUnboundMemSidePort();
    CPUSidePort *getUnboundCPUSidePort();

    void connect(RAM *ram) {
        MasterPort *p = getUnboundMemSidePort();
        // p->bind(ram->getPort());
    }
};

#endif  // __MEMBUS_HH__