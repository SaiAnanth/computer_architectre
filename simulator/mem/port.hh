#ifndef __PORTS_HH__
#define __PORTS_HH__

#include "packet.hh"

class SlavePort {
    friend class MasterPort;

   private:
    MasterPort* mport;
    Packet* activeRequest;

   public:
    std::string name;
    SlavePort(const std::string& n) : name(n) {}
    void bind(MasterPort* _mport);
    void unbind();
    bool isBound() { return (mport != NULL); }
    bool isBusy() { return activeRequest != NULL; }
    MasterPort* getMaster() { return mport; }

    virtual void sendResp(Packet* pkt);
    virtual void recvReq(Packet* pkt) = 0;
    virtual const AddrRange getAddrRange() = 0;
};

class MasterPort {
    friend class SlavePort;

   private:
    SlavePort* sport;
    Packet* activeRequest;

   public:
    std::string name;
    MasterPort(const std::string& n) : name(n) {}
    void bind(SlavePort* _sport);
    void unbind();
    bool isBound() { return (sport != NULL); }
    SlavePort* getSlave() { return sport; }
    bool isBusy() { return activeRequest != NULL; }
    virtual void sendReq(Packet* pkt);
    virtual void recvResp(Packet* pkt) = 0;
    const AddrRange getAddrRange() { return sport->getAddrRange(); }
};

#endif  // __PORTS_HH__