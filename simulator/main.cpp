
using namespace std;

#include "simulations/simulation1.hh"
#include "simulations/simulation2.hh"
#include "simulations/simulation3.hh"
#include "simulations/simulation4.hh"

int main() {
    simulation1 *sim1 = new simulation1();
    simulation2 *sim2 = new simulation2();
    sim2->run();

    simulation3 *sim3 = new simulation3();
    sim3->run();

    simulation4 *sim4 = new simulation4();
    sim4->run();

    sim1->run();
    sim2->validate();

    sim3->validate();

    sim4->validate(true);
}