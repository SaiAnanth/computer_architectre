#ifndef __SYSTEM_HH__
#define __SYSTEM_HH__

#include <vector>

#include "../base/event.hh"

class System {
   private:
    Tick currentTick;           // To keep track of the ticks
    Tick finalTick;             // TO check for end tick
    std::vector<Event *> MEQ;   // Event Queue
    std::vector<Event *>::iterator findEvent(Event *e);

   public:
    System() : currentTick(0) {}
    void schedule(Event *e, Tick t);
    void reschedule(Event *e, Tick t);
    void runSimulation(Tick endTick = -1);
    Tick currTick() { return currentTick; }
    void printMEQ();
    Event *popEvent();
    void setFinalTick(Tick final) { finalTick = final; }
};

#endif  //__SYSTEM_HH__