#include "system.hh"

#include <cassert>
#include <iostream>

// Find the given Event in the queue
std::vector<Event *>::iterator System::findEvent(Event *e) {
    for (auto it = MEQ.begin(); it != MEQ.end(); it++) {
        if (*it == e) {
            std::cout << "Event found\n";
            return it;
        }
    }
    return MEQ.end();
}

// schedule the Event in the queue for given tick
void System::schedule(Event *e, Tick t) {
    assert(t >= currentTick);
    // std::cout << "Attempting to schedule " << e->description() << " at time "
    //           << t << std::endl;
    if (!(e->isScheduled())) {
        e->schedule(t);
        for (auto it = MEQ.begin(); it != MEQ.end(); it++) {
            if (e->time() < (*it)->time()) {
                MEQ.insert(it, e);
                return;
            }
        }
        MEQ.push_back(e);
    } else {
        std::cout << "ERROR: Event already scheduled. Cannot be rescheduled.\n";
        assert(0);
    }
}

// reschedule a scheduled event to a new tick
void System::reschedule(Event *e, Tick t) {
    assert(t >= currentTick);
    // std::cout << "Attempting to schedule " << e->description() << " at time "
    //           << t << std::endl;
    if (e->isScheduled() && t < e->time()) {
        MEQ.erase(findEvent(e));
        e->schedule(t);
        for (auto it = MEQ.begin(); it != MEQ.end(); it++) {
            if (e->time() < (*it)->time()) {
                MEQ.insert(it, e);
                return;
            }
        }
        MEQ.push_back(e);
    }
}

// the main method to run the simulations
void System::runSimulation(Tick endTick) {
    finalTick = endTick;
    while (!(MEQ.empty()) && (currentTick <= finalTick)) {
        // std::cout << "Simulation Tick: " << currentTick << std::endl;
        // printMEQ();
        while (MEQ.begin() != MEQ.end()) {
            // printMEQ();
            if (MEQ.front()->time() < currentTick) {
                std::cout << "Event was scheduled prior to currentTick\n";
                assert(0);
            } else if (MEQ.front()->time() == currentTick) {
                popEvent()->process();
            } else {
                break;
            }
        }
        currentTick++;
    }
}

// print the queue for debugging
void System::printMEQ() {
    std::cout << "Start of MEQ\n";
    for (auto e : MEQ) {
        std::cout << e->time() << ":" << e->description() << std::endl;
    }
    std::cout << "End of MEQ\n";
}

// remove the events from the queue
Event *System::popEvent() {
    Event *tmp = MEQ.front();
    tmp->deschedule();
    MEQ.erase(MEQ.begin());
    return tmp;
}