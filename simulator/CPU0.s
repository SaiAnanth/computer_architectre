main:                                   # Addr 0x0
	addi	sp, sp, -16					# 0XFF010113	0
	sw	ra, 12(sp)						# 0X00112623	4
	sw	s0, 8(sp)						# 0X00812423	8
	addi	s0, sp, 16					# 0X01010413 	C
	mv	a0, zero # addi a0, zero,0		# 0X00000513	10
	sw	a0, -12(s0)						# 0XFEA42A23	14
	sw	a0, -16(s0)						# 0XFEA42823	18
	j	.LBB0_1   # jal PC, 0x20		# 0X0200006F	1C
.LBB0_1:           # Addr 0x20
	lw	a0, -16(s0)						# 0XFF042503
	addi	a1, zero, 255				# 0X0FF00593
	blt	a1, a0, .LBB0_4					# 0X08A5C063
	j	.LBB0_2  # jal PC, 0x30			# 0X0300006F
.LBB0_2:               # Addr 0x30
	lui	a0, %hi(1024)					# 0X00000537
	addi  a0, a0, %lo(1024)				# 0X40050513
	lw	a1, -16(s0)						# 0XFF042583
	slli	a1, a1, 2    # a1 = a1 * 4	# 0X00259593
	add	a0, a0, a1						# 0X00B50533
	flw	ft0, 0(a0)						# 0X00052007
	lui	a0, %hi(2048)					# 0X00001537
	addi  a0, a0, %lo(2048)				# 0X80050513
	add	a0, a0, a1						# 0X00B50533
	flw	ft1, 0(a0)						# 0X00052087
	fadd.s	ft0, ft0, ft1				# 0X00100053
	lui	a0, %hi(3072)					# 0X00000537
	addi  a0, a0, %lo(3072)				# 0XC0050513
	add	a0, a0, a1						# 0X00B50533
	fsw	ft0, 0(a0)						# 0X00052027
	j	.LBB0_3    # jal PC, 0x70		# 0X0700006F
.LBB0_3:              # Addr 0x70
	lw	a0, -16(s0)						# 0XFF042503
	addi	a0, a0, 1					# 0X00150513
	sw	a0, -16(s0)						# 0XFEA42823
	j	.LBB0_1   # jal PC, 0x20		# oX0200006F
.LBB0_4:				# Addr 0x80    
	lw	a0, -12(s0)						# 0XFF442503
	lw	s0, 8(sp)						# 0X00812403
	lw	ra, 12(sp)						# 0X00C12083
	addi	sp, sp, 16					# 0X01010113
	ret									# 0X00008067
