#ifndef __PIPELINE_STATUS_H__
#define __PIPELINE_STATUS_H__

enum PipeLineStatus {
    IDLE,
    READWRITE,
    READWRITECOMPLETE,
    PROCESSING,
    STALLING,
    COMPLETED,
    ERROR,
    END
};

#endif