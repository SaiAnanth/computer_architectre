#ifndef __EXECUTE_H__
#define __EXECUTE_H__

#include "cache.hh"
#include "cpu.hh"
#include "pipeline_stage.hh"

namespace CPU {
class Execute : public PipelineStage {
   private:
    Cpu *cpu;
    uint32_t instCount;
    System *sys;

    class DataPort : public MasterPort {
       private:
        Execute *execute;

       public:
        DataPort(Execute *e) : MasterPort(""), execute(e) {}
        void recvResp(Packet *pkt) { execute->recvResp(pkt); }
    };
    DataPort *dport;
    Cache *dcache;

   public:
    bool jumpDetected;
    bool endExecution;
    Execute(Cpu *c, System *s) : cpu(c), sys(s), dport(new DataPort(this)) {}

    void setdataCache(uint32_t size, uint32_t blocksize, Associativity type,
                      uint32_t time) {
        dcache = new Cache(sys, size, blocksize, type, time);
        dport->bind(dcache->cpuPort);
    }

    MasterPort *getDataPort() { return dport; }
    MasterPort *getDataCachePort() { return dcache->memPort; }

    uint32_t getInstCount() { return instCount; }

    void emptyCache() {
        if (dcache) {
            dcache->emptyCache();
        }
    }

    bool process() {
        if (!isempty()) {
            if (status == IDLE) {
                inst->dport = dport;
                status = inst->read_write();
            }

            if (status == READWRITECOMPLETE) {
                status = inst->execute();
                instCount++;
                if (inst->isjump) {
                    // when there is a jump flush the pipeline
                    cpu->flushPipeline();
                    jumpDetected = true;
                    return true;
                }
                if (inst->ret) {
                    return false;
                }
            }
            // if stall wait for the cycle 
            if (status == STALLING) {
                inst->wait();
                if (inst->checkStall()) {
                    status = COMPLETED;
                }
            }
            if (status == COMPLETED) {
                if (next && next->isempty()) {
                    next->setInstruction(inst);
                    inst = nullptr;
                    status = IDLE;
                }
            }
        }
        return true;
    }
    // store the received response in the result
    void recvResp(Packet *pkt) {
        if (status == READWRITE) {
            std::cout << *(int *)pkt->data << std::endl;
            inst->result = *(uint32_t *)pkt->data;
            inst->fresult = *(float *)pkt->data;
            status = READWRITECOMPLETE;
        }
    }
};

}  // namespace CPU

#endif  // __EXECUTE_H__