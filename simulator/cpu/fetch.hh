#ifndef __FETCH_H__
#define __FETCH_H__

#include "../riscv/registers.hh"
#include "cache.hh"
#include "cpu.hh"
#include "pipeline_stage.hh"

namespace CPU {
class Fetch : public PipelineStage {
   private:
    Cpu *cpu;
    Cache *cache;
    System *sys;
    class InstPort : public MasterPort {
       private:
        Fetch *fetch;

       public:
        InstPort(Fetch *f) : MasterPort("CPU Instr Port\n"), fetch(f) {}
        void recvResp(Packet *pkt) { fetch->recvResp(pkt); }
    };
    InstPort *iport;

   public:
    Fetch(Cpu *c, System *s) : cpu(c), sys(s), iport(new InstPort(this)) {}

    bool process() {
        //  fetch  an instruction if the stage is idle or not busy
        if (status == IDLE && !isBusy()) {
            fetchInst();
        }
        // if it is completed check if the next state is empty to receive the
        // instruction
        if (status == COMPLETED) {
            if (next && next->isempty()) {
                next->setInstruction(inst);
                inst = nullptr;
                fetchInst();
                status = IDLE;
            }
        }
        return true;
    }

    MasterPort *getInstPort() { return iport; }
    MasterPort *getInstCachePort() { return cache->memPort; }

    bool isBusy() {
        // check if the fetch is busy or not
        if (cache) {
            return cache->isBusy();
        } else {
            return iport->isBusy();
        }
    }
    void setInstCache(uint32_t size, uint32_t blocksize, Associativity type,
                      uint32_t time) {
        cache = new Cache(sys, size, blocksize, type, time);
        iport->bind(cache->cpuPort);
    }
    // request for instruction from PC of 4 bytes
    void fetchInst() {
        printf("requesting instruction\n");
        Packet *pkt = new Packet(false, cpu->regs[PC], 4);
        iport->sendReq(pkt);
        status = PROCESSING;
    }

    // when received resp take the instruction and increment PC by 4 and ste the
    // status to completed
    void recvResp(Packet *pkt) {
        if (status == PROCESSING && pkt->addr == cpu->regs[PC]) {
            inst = new Instruction(cpu->regs, cpu->fregs);
            inst->reg = *(uint32_t *)pkt->data;
            printf("fetched instruction %X \n", *(int *)pkt->data);
            cpu->regs[PC] = cpu->regs[PC] + 4;
            status = COMPLETED;
        }
    }
};
}  // namespace CPU

#endif