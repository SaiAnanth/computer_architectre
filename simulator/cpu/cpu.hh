#ifndef __CPU_H__
#define __CPU_H__

#include <iostream>
#include <vector>

#include "../base/simobject.hh"
#include "../base/tickevent.hh"
#include "../mem/membus.hh"
#include "../mem/ram.hh"
#include "cache.hh"
#include "pipeline_stage.hh"

namespace CPU {
class Pipeline;
}

class Cpu : public Simobject {
   private:
    TickEvent *tickevent;                   // Keep track of the cup event
    CPU::Pipeline *pipeline;                // holds the cpu pipeline
    std::string name;                       // name of the cpu for Debugging
    uint32_t latency;                       // time for next cpu cycle 

   public:
    uint32_t regs[33];                      // last reg is used for PC
    float fregs[32];                        // used to store floating point values
    uint32_t cycles;                        // keep track of the number of cycles for the cpu
    Cpu(System *s, const std::string &n);

    void init();

    void process();
    void setParams(uint32_t, uint32_t);     // set the sp and pc for the cpu
    void flushPipeline();                   // empty the pipeline
    float CPI();                            // calculates the cpi of the CPU
    int32_t getInstructionCount();          // will return the number of instruction executed by the cpu

    MasterPort *getInstPort();              // gets the instruction port
    MasterPort *getInstCachePort();         // gets the instruction port with cache
    MasterPort *getDataPort();              // gets the data port with cache
    MasterPort *getDataCachePort();         // gets the data port with cache

    void setInstCache(uint32_t, uint32_t, Associativity, uint32_t = 0);

    void setDataCache(uint32_t, uint32_t, Associativity, uint32_t = 0);
};

#endif