#ifndef __CACHE_H__
#define __CACHE_H__

#include <assert.h>
#include <tgmath.h>

#include <cstdint>
#include <cstring>
#include <iostream>
#include <vector>

#include "../base/simobject.hh"
#include "../base/tickevent.hh"
#include "../mem/packet.hh"
#include "../mem/port.hh"
#include "../riscv/inst_const.hh"
#include "../system/system.hh"

#define VALID 1 << 0
#define MRU 1 << 1
#define DIRTY 1 << 2

#define GETBIT(n, k) (n & (1 << k)) >> k
#define SETBIT(n, k) (1 << k) | n
#define CLEARBIT(n, k) (n & (~(1 << k)))

enum Associativity { DIRECT, WAY_2, WAY_4, FULLY };
class Cache : public Simobject {
   public:
   private:
    uint32_t size;
    uint32_t blockSize;
    Associativity type;
    uint32_t blockcount;
    std::vector<uint32_t> flags;
    std::vector<uint32_t> tags;
    uint8_t* cachemem;
    Packet* activeRequest;
    uint32_t offsetBits;
    std::vector<uint32_t> accessedMemblock;
    TickEvent* tickEvent;

    std::vector<Packet*> writeBackBuffer;

   public:
    class CPURespEvent : public Event {
       private:
        Cache* cache;

       public:
        CPURespEvent(Cache* c) : Event(), cache(c) {}
        void process() { cache->sendCPUResp(); }
        const char* description() override { return "CPU Cache event"; }
    };

    class CPUPort : public SlavePort {
       private:
        Cache* cache;

       public:
        CPUPort(Cache* c) : SlavePort("Cache CPU port"), cache(c) {}

        void recvReq(Packet* pkt) override { cache->recvCPUReq(pkt); }
        const AddrRange getAddrRange() override {
            return cache->memPort->getAddrRange();
        }
    };

    class MemPort : public MasterPort {
       private:
        Cache* cache;

       public:
        MemPort(Cache* c) : MasterPort("Cache Mem Port"), cache(c) {}
        void recvResp(Packet* pkt) override { cache->recvMemResp(pkt); }
    };

    uint32_t hitcount;
    uint32_t misscount;
    uint32_t coldmisscount;
    uint32_t lookupTime;

    CPUPort* cpuPort;
    MemPort* memPort;

   public:
    Cache(System* sys, uint32_t s, uint32_t bs, Associativity a,
          uint32_t ltime = 0)
        : Simobject(sys),
          size(s),
          blockSize(bs),
          type(a),
          lookupTime(ltime),
          cpuPort(new CPUPort(this)),
          memPort(new MemPort(this)),
          tickEvent(new TickEvent(this)) {
        init();
    }

    void init() {
        blockcount = size / blockSize;
        cachemem = new uint8_t[size];
        offsetBits = (uint32_t)log2(blockSize);
        if (lookupTime == 0) {
            lookupTime = ceil(log2(blockcount));
            switch (type) {
                case DIRECT: {
                    lookupTime = lookupTime * 1;
                    break;
                }
                case FULLY: {
                    lookupTime = lookupTime * blockcount;
                    break;
                }
                case WAY_2: {
                    lookupTime = lookupTime * 2;
                    break;
                }
                case WAY_4: {
                    lookupTime = lookupTime * 4;
                    break;
                }
            }
        }
        for (int i = 0; i < blockcount; i++) {
            flags.push_back(0);
            tags.push_back(-1);
        }
        schedule(tickEvent, currTick());
    }

    bool isBusy() { return cpuPort->isBusy() || memPort->isBusy(); }

    void emptyCache() {
        for (int block = 0; block < blockcount; block++) {
            if (GETBIT(flags[block], DIRTY)) {
                writeBackBuffer.push_back(writeBackBlock(block));
            }
        }
        schedule(tickEvent, currTick() + 1);
    }

    void process() {
        if (!isBusy() && writeBackBuffer.size() != 0) {
            Packet* pkt = writeBackBuffer.front();
            writeBackBuffer.erase(writeBackBuffer.begin());
            memPort->sendReq(pkt);
        }
        if (writeBackBuffer.size() != 0) {
            schedule(new TickEvent(this), currTick() + 1);
        }
    }

    void read(uint32_t addr) {
        int cacheBlock = check(addr);
        if (cacheBlock >= 0 && GETBIT(flags[cacheBlock], VALID)) {
            hitcount++;
        } else {
            cacheBlock = getNewBlock(addr);
            assert(cacheBlock < blockcount);
            uint32_t memb = getMemBlocknumber(addr);
            int i = 0;
            for (i = 0; i < accessedMemblock.size(); i++) {
                if (memb == accessedMemblock[i]) {
                    break;
                }
            }
            if (i == accessedMemblock.size()) {
                coldmisscount++;
                accessedMemblock.push_back(i);
            }
            misscount++;
            flags[cacheBlock] = SETBIT(flags[cacheBlock], VALID);
            tags[cacheBlock] = getTag(addr);
        }
    }

   private:
    uint32_t getMemBlocknumber(uint32_t addr) { return addr / blockSize; }
    uint32_t getOffset(uint32_t addr) {
        return GETBITS(addr, 0, offsetBits - 1);
    }
    uint32_t getIndex(uint32_t addr, uint32_t way) {
        uint32_t setCount = blockcount / way;
        uint32_t n = getMemBlocknumber(addr);
        return n % setCount;
    }

    uint32_t getTag(uint32_t addr) {
        uint32_t offsetBitCount = (uint32_t)log2(blockSize);
        addr = addr >> offsetBitCount;
        switch (type) {
            case DIRECT: {
                return addr;
            }
            case FULLY: {
                return addr;
            }
            case WAY_2: {
                uint32_t setCount = blockcount / 2;
                uint32_t setBitCount = (uint32_t)log2(setCount);
                return addr >> setBitCount;
            }
            case WAY_4: {
                uint32_t setCount = blockcount / 4;
                uint32_t setBitCount = (uint32_t)log2(setCount);
                return addr >> setBitCount;
            }
        }
        printf("invalid type\n");
        assert(0);
    }

    uint32_t getMemaddr(int block) {
        uint32_t tag = tags[block];
        tag = tag << offsetBits;
        switch (type) {
            case WAY_2: {
                uint32_t indxcnt = ceil(log2(blockcount / 2));
                tag = tag << indxcnt;
                uint32_t indx = block / 2;
                tag = SETBITS(tag, indx, indxcnt + offsetBits - 1, offsetBits);
                break;
            }
            case WAY_4: {
                uint32_t indxcnt = ceil(log2(blockcount / 4));
                tag = tag << indxcnt;
                uint32_t indx = block / 4;
                tag = SETBITS(tag, indx, indxcnt + offsetBits - 1, offsetBits);
                break;
            }
        }
        return tag;
    }

    int check(uint32_t addr) {
        uint32_t tag = getTag(addr);
        switch (type) {
            case DIRECT: {
                // direct just check one block tag
                uint32_t n = getMemBlocknumber(addr);
                uint32_t b = n % blockcount;

                if (tag == tags[b]) {
                    return b;
                }
            }
            case FULLY: {
                // check all the tags in cache
                for (int i = 0; i < blockcount; i++) {
                    if (tag == tags[i]) {
                        return i;
                    }
                }
            }
            case WAY_2: {
                // check all the tags in that set
                uint32_t set = getIndex(addr, 2);
                for (int i = set * 2; i < (set * 2) + 2; i++) {
                    if (tag == tags[i]) {
                        return i;
                    }
                }
            }
            case WAY_4: {
                // check all the tags in that set
                uint32_t set = getIndex(addr, 4);
                for (int i = set * 4; i < (set * 4) + 4; i++) {
                    if (tag == tags[i]) {
                        return i;
                    }
                }
            }
        }
        return -1;
    }

    uint32_t getNewBlock(uint32_t addr) {
        uint32_t memBlock = getMemBlocknumber(addr);
        switch (type) {
            case DIRECT: {
                return memBlock % blockcount;
            }
            case FULLY: {
                return PLRU(0, blockcount);
            }
            case WAY_2: {
                return nWayAssociative(addr, 2);
            }
            case WAY_4: {
                return nWayAssociative(addr, 4);
            }
        }
        printf("invalid type\n");
        assert(0);
    }

    uint32_t nWayAssociative(uint32_t addr, uint32_t setSize) {
        uint32_t set = getIndex(addr, setSize);
        return PLRU((set * setSize), (set * setSize) + setSize - 1);
    }

    uint32_t PLRU(uint32_t start, uint32_t end) {
        // Bit-PLRU
        int i = start;
        // Check for recently used bit
        for (i = start; i <= end; i++) {
            if (GETBIT(flags[i], MRU) == 0) {
                break;
            }
        }
        // if non found reset all
        if (i == end) {
            for (i = start; i <= end; i++) {
                flags[i] = CLEARBIT(flags[i], MRU);
            }
            i = start;
        }
        // take the first block and set the bit and return
        flags[i] = SETBIT(flags[i], MRU);
        return i;
    }

    void setBlock(uint32_t blockno, uint8_t* data, uint32_t s) {
        uint32_t addr = (blockno * blockSize);
        memcpy((cachemem + addr), data, s);
    }

    void printCache() { printdata(cachemem, size); }

    void printdata(uint8_t* data, uint32_t s) {
        for (int i = 0; i < s; i += 4) {
            printf("cache data at %X is %X\n", i, *(uint32_t*)(data + i));
        }
    }

    void readhit(uint32_t addr, uint32_t block, uint32_t s, uint8_t* data) {
        // get the offset
        uint32_t offset = getOffset(addr);
        // get the cache block addr
        uint32_t cacheaddr = (block * blockSize) + offset;
        // copy the data
        memcpy(data, (cachemem + cacheaddr), s);
    }

    void writehit(uint32_t addr, uint32_t block, uint32_t s, uint8_t* data) {
        // get the offset
        uint32_t offset = getOffset(addr);
        // get the cache block addr
        uint32_t cacheaddr = (block * blockSize) + offset;
        // copy the data from packet
        memcpy((cachemem + cacheaddr), data, s);
        // set dirty bit
        flags[block] = SETBIT(flags[block], DIRTY);
        printf("write hit for addr %X for block %d data is %d\n", addr, block,
               *(uint32_t*)(cachemem + cacheaddr));
    }

    void recvCPUReq(Packet* pkt) {
        printf("received request from CPU for addr %X \n", pkt->addr);
        activeRequest = pkt;
        // Check if the block is valid
        int block = check(pkt->addr);
        if (block >= 0) {
            // hit
            if (pkt->isWrite()) {
                // write hit
                writehit(pkt->addr, block, pkt->size, pkt->data);
            } else {
                // read hit
                readhit(pkt->addr, block, pkt->size, pkt->data);
            }
            // schedule a response Event with latency
            schedule(new CPURespEvent(this), currTick() + lookupTime);
        } else {
            // Treat write miss as read miss and then update the cache
            printf("miss for address %X\n", pkt->addr);
            sendMemReq(pkt->addr);
        }
    }

    void sendCPUResp() {
        printf("Send Resp to CPU for addr %X \n", activeRequest->addr);
        // send active packet over cpu port;
        cpuPort->sendResp(activeRequest);
        activeRequest = nullptr;
    }

    void sendMemReq(uint32_t addr) {
        printf("Send Request to Mem for addr %X\n", addr);
        // get the memory block number and start addr
        uint32_t memBlockno = getMemBlocknumber(addr);
        uint32_t memBlockStart = memBlockno * blockSize;
        Packet* memPkt = new Packet(false, memBlockStart, blockSize);
        // send it via the mem port
        memPort->sendReq(memPkt);
    }

    Packet* writeBackBlock(uint32_t block) {
        // get the memory addr from block
        uint32_t memaddr = getMemaddr(block);
        // get the cache offset for the block
        uint32_t cacheaddr = block * blockSize;
        // create a new packet and copy the data
        Packet* writebackPkt = new Packet(true, memaddr, blockSize);
        for (int i = 0; i < blockSize; i += 4) {
            memcpy((writebackPkt->data + i), (cachemem + cacheaddr + i), 4);
        }
        // clear the dirty bit
        flags[block] = CLEARBIT(flags[block], DIRTY);
        return writebackPkt;
    }
    void recvMemResp(Packet* pkt) {
        if (!pkt->isWrite()) {
            printf("received Resp from Mem for addr %X\n", pkt->addr);
            // get the repalcement block
            uint32_t newBlock = getNewBlock(pkt->addr);
            // if the block is dirty write back the data
            if (GETBIT(flags[newBlock], DIRTY)) {
                // put the packet in write back buffer and schedule the cache
                writeBackBuffer.push_back(writeBackBlock(newBlock));
                schedule(tickEvent, currTick() + 1);
            }
            // set the new data in the block
            setBlock(newBlock, pkt->data, pkt->size);
            // update the tag and valid block
            tags[newBlock] = getTag(pkt->addr);
            flags[newBlock] = SETBIT(flags[newBlock], VALID);
            // If there is any active request from CPU serve it
            if (activeRequest) {
                recvCPUReq(activeRequest);
            }
        }
    }
};

#endif  // __CACHE_H__