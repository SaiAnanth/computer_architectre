#ifndef __PIPELINESTAGE_H__
#define __PIPELINESTAGE_H__

#include "../riscv/instruction.hh"
#include "pipeline_status.hh"

class PipelineStage {
   protected:
    Instruction* inst;
    PipelineStage* next;

   public:
    PipeLineStatus status;
    PipelineStage() { status = IDLE; }
    void setInstruction(Instruction* i) { inst = i; }
    void setNext(PipelineStage* n) { next = n; }
    void reset() { status = IDLE; }
    bool isempty() {
        if (inst) {
            return inst->isempty();
        } else {
            return true;
        }
    }
    virtual bool process() = 0;
};

#endif