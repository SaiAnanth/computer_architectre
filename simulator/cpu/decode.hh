#ifndef __DECODE_H__
#define __DECODE_H__

#include "../riscv/rv32f.hh"
#include "../riscv/rv32i.hh"
#include "cpu.hh"
#include "pipeline_stage.hh"
namespace CPU {
class Decode : public PipelineStage {
   private:
    Cpu* cpu;

   public:
    Decode(Cpu* c) : cpu(c) {}

    Instruction* getInstSubClass(Instruction* inst) {
        // get the opcode func3 and func7 to decode the instruction  and create
        // a new object for the specific instruction
        uint8_t opcode = GETBITS(inst->reg, 0, 6);
        uint8_t func3 = GETBITS(inst->reg, 12, 14);
        uint8_t func7 = GETBITS(inst->reg, 25, 31);

        printf("decoding instruction %X ", inst->reg);
        switch (opcode) {
                // ADDI,SLLI

            case ADDI: {
                if (func3 == 0X0) {
                    printf("addi ");
                    return new Addi(inst->regs, inst->fregs, inst->reg);
                }
                if (func3 == 0X001) {
                    printf("slli ");
                    return new Slli(inst->regs, inst->fregs, inst->reg);
                }
                break;
            }
            case LW: {
                if (func3 == 0b010) {
                    return new Lw(inst->regs, inst->fregs, inst->reg);
                }
            }
            case SW: {
                if (func3 == 0b010) {
                    printf("sw ");
                    return new Sw(inst->regs, inst->fregs, inst->reg);
                }
            }
            case LUI: {
                printf("lui ");
                return new Lui(inst->regs, inst->fregs, inst->reg);
            }
            case ADD: {
                if (func3 == 0b000 && func7 == 0b0000000) {
                    printf("add ");
                    return new Add(inst->regs, inst->fregs, inst->reg);
                }
            }
            case JAL: {
                printf("JAL ");
                return new Jal(inst->regs, inst->fregs, inst->reg);
            }
            case JALR: {
                printf("JALR ");
                return new Jalr(inst->regs, inst->fregs, inst->reg);
            }
            case BLT: {
                if (func3 == 0b100) {
                    printf("BLT ");
                    return new Blt(inst->regs, inst->fregs, inst->reg);
                }
            }
            case FLW: {
                if (func3 == 0b010) {
                    printf("FLW ");
                    return new Flw(inst->regs, inst->fregs, inst->reg);
                }
            }
            case FADDS: {
                printf("FADDS ");
                return new Fadds(inst->regs, inst->fregs, inst->reg);
            }
            case FSW: {
                return new Fsw(inst->regs, inst->fregs, inst->reg);
            }

            default:
                break;
        }
        return nullptr;
    }
    // try to decode the instruction and send to next stage if it is empty
    bool process() {
        if (!isempty()) {
            if (status == IDLE) {
                inst = getInstSubClass(inst);
                if (inst) {
                    inst->decode();
                    inst->dispInst();
                } else {
                    status = ERROR;
                    std::cout << "failed to decode instruction\n";
                    assert(0);
                }
                status = COMPLETED;
            }
            if (status == COMPLETED) {
                if (next && next->isempty()) {
                    next->setInstruction(inst);
                    inst = nullptr;
                    status = IDLE;
                }
            }
        }

        return true;
    }
};
}  // namespace CPU

#endif  // __DECODE_H__