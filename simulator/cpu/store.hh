#ifndef __STORE_H__
#define __STORE_H__

#include "cpu.hh"
#include "pipeline_stage.hh"

namespace CPU {
class Store : public PipelineStage {
   private:
    Cpu* cpu;

   public:
    Store(Cpu* c) : cpu(c) {}
    bool process() {
        if (!isempty()) {
            if (status == IDLE) {
                inst->store();
                status = COMPLETED;
            }
            if (status == COMPLETED) {
                inst = nullptr;
                status = IDLE;
            }
        }
        return true;
    }
};
}  // namespace CPU
#endif  // __STORE_H__