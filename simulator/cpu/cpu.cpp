#include "cpu.hh"

#include "pipeline.hh"

Cpu::Cpu(System *s, const std::string &n)
    : Simobject(s),
      name(n),
      pipeline(new CPU::Pipeline(this, s)),
      tickevent(new TickEvent(this)),
      latency(10) {
    init();
}

void Cpu::init() {
    printf("schdeuling cpu %s\n", name.c_str());
    cycles++;
    schedule(tickevent, currTick());
}

void Cpu::setParams(uint32_t stackp, uint32_t pc) {
    regs[sp] = stackp;
    regs[PC] = pc;
}

void Cpu::process() {
    std::cout << "\n------ at Tick " << name << "  " << currTick() << " ------"
              << std::endl;
    // process every time and reschedule it to next cycle
    if (pipeline->process()) {
        cycles++;
        schedule(tickevent, currTick() + latency);
    } else {
        // empty the cache if the execution of cpu is completed
        pipeline->emptyCache();
    }
}

void Cpu::flushPipeline() { pipeline->flushPipeline(); }

float Cpu::CPI() { return cycles / pipeline->instCount(); }

int32_t Cpu::getInstructionCount() { return pipeline->instCount(); }

MasterPort *Cpu::getInstPort() { return pipeline->getInstPort(); }
MasterPort *Cpu::getInstCachePort() { return pipeline->getInstCachePort(); }
MasterPort *Cpu::getDataPort() { return pipeline->getDataPort(); }
MasterPort *Cpu::getDataCachePort() { return pipeline->getDataCachePort(); }

void Cpu::setInstCache(uint32_t size, uint32_t blocksize, Associativity type,
                       uint32_t time) {
    pipeline->setInstCache(size, blocksize, type, time);
}

void Cpu::setDataCache(uint32_t size, uint32_t blocksize, Associativity type,
                       uint32_t time) {
    pipeline->setDataCache(size, blocksize, type, time);
}