#ifndef __PIPELINE_H__
#define __PIPELINE_H__

#include "decode.hh"
#include "execute.hh"
#include "fetch.hh"
#include "store.hh"

namespace CPU {
class Pipeline {
   private:
    Cpu* cpu;

   protected:
    Decode* decode;
    Execute* execute;
    Store* store;

   public:
    Fetch* fetch;
    Pipeline(Cpu* c, System* s)
        : cpu(c),
          fetch(new Fetch(c, s)),
          decode(new Decode(c)),
          execute(new Execute(c, s)),
          store(new Store(c)) {
        // sets the four stage pipeline fetch -> decode -> execute -> store
        fetch->setNext(decode);
        decode->setNext(execute);
        execute->setNext(store);
    }
    bool process() {
        // process the stages from the back
        store->process();
        bool notEnd = execute->process();
        if (execute->jumpDetected) {
            execute->jumpDetected = false;
            return notEnd;
        }
        decode->process();
        fetch->process();
        return notEnd;
    }
    MasterPort* getInstPort() { return fetch->getInstPort(); }
    MasterPort* getInstCachePort() { return fetch->getInstCachePort(); }
    MasterPort* getDataPort() { return execute->getDataPort(); }
    MasterPort* getDataCachePort() { return execute->getDataCachePort(); }
    void flushPipeline() {
        // empty the instructions in all the stages and reset the states
        fetch->setInstruction(nullptr);
        decode->setInstruction(new Instruction(cpu->regs, cpu->fregs));
        execute->setInstruction(new Instruction(cpu->regs, cpu->fregs));
        store->setInstruction(new Instruction(cpu->regs, cpu->fregs));
        fetch->reset();
        decode->reset();
        execute->reset();
        store->reset();
    }
    int32_t instCount() { return execute->getInstCount(); }

    void emptyCache() { execute->emptyCache(); }

    void setInstCache(uint32_t size, uint32_t blocksize, Associativity type,
                      uint32_t time) {
        fetch->setInstCache(size, blocksize, type, time);
    }
    void setDataCache(uint32_t size, uint32_t blocksize, Associativity type,
                      uint32_t time) {
        execute->setdataCache(size, blocksize, type, time);
    }
};
}  // namespace CPU

#endif